# load_gmat.py
# This file is a template for files used run the GMAT API from a folder outside  
# of the GMAT application folder.

import os
from os import path
import sys

api_startup_file = "api_startup_file.txt"
gmat_base_directory = r"C:\Software\GMAT"
gmat_bin_directory = gmat_base_directory + '/bin'
Startup = gmat_bin_directory + '/' + api_startup_file

if path.exists(Startup):

   sys.path.insert(1, gmat_bin_directory)

   import gmatpy as gmat
   gmat.Setup(Startup)

else:
   print("Cannot find ", Startup)
   print()
   print("Please set up a GMAT startup file named ", api_startup_file, " in the ", 
      gmat_bin_directory, " folder.")
