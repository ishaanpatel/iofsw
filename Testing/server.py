# server.py
#
# The primary driver that handles the server (ground station) interaction with the client (spacecraft).
#
########################################################################################################
import socket                                         
import time

# Constants
HEADER_SIZE = 10

# Create a socket object (AF_INET == ipv4, SOCK_STREAM == TCP)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 

# Obtain local machine name & port
host_name = socket.gethostname()                           
port = 12345                                          

# Bind to the port
server_socket.bind((host_name, port))                                  

# Queue up to 5 requests
server_socket.listen(5)

def generate_heartbeat_message():
    message = 'Current Time: ' + time.ctime(time.time())
    message = f'{len(message):<{HEADER_SIZE}}' + message
    return message

while True:
    # Establish a connection
    client_socket, address = server_socket.accept()
    print(f"Connection from {address} has been established.") 

    while True:
        time.sleep(3)

        heartbeat_message = generate_heartbeat_message()

        print('[GROUND] Sending heartbeat message.') 
        client_socket.send(bytes(heartbeat_message, 'utf-8'))
    
    # client_socket.close()