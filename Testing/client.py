# client.py
#
# The primary driver that handles the client (spacecraft) interaction with the server (ground station).
#
########################################################################################################
import socket

# Constants
HEADER_SIZE = 10

# Create a socket object (AF_INET == ipv4, SOCK_STREAM == TCP)
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 

# Obtain local machine name & port
host_name = socket.gethostname()                           
port = 12345 

# Connect to the server socket
client_socket.connect((host_name, port))

while True:
    full_message = ''
    is_new_message = True

    while True:
        message = client_socket.recv(16)

        if is_new_message:
            print('New message length: ', message[:HEADER_SIZE])
            message_length = int(message[:HEADER_SIZE])
            is_new_message = False

        print(f'Full message length: {message_length}')

        full_message += message.decode('utf-8')

        print(len(full_message))

        if len(full_message)-HEADER_SIZE == message_length:
            print('[SPACECRAFT] Full message received.')
            print(full_message[HEADER_SIZE:])
            is_new_message = True
            full_message = ''