# In-Orbit Master controller
#
# Ishaan Patel
#
# This file is the main simulator for the IOFSW. 
# It configures a propapd_propagator using the GMAT API and uses it to advance an initial state in 60 second increments.

#%% Initialize
from load_gmat_iofsw import *
import os
import sys
import numpy as np
# sys.path.append(r'C:\Software\GMAT\api')
# sys.path.append('..')

import datetime

import matplotlib.pyplot as plt


#%%

# GMAT base path
GMAT_BASE_PATH = r'C:\Software\GMAT'
EPOCH_STR_FORMAT = "%d %b %Y %H:%M:%S.000"

# Spacecraft configuration preliminaries
haven = gmat.Construct("Spacecraft", "Haven")
initial_epoch = datetime.datetime(year=2020, month=8, day=1, hour=12, minute=00, second=00)
haven.SetField("DateFormat", "UTCGregorian")
haven.SetField("Epoch", initial_epoch.strftime(EPOCH_STR_FORMAT))

haven.SetField("CoordinateSystem", "EarthMJ2000Eq")
haven.SetField("DisplayStateType", "Keplerian")

# Orbital state
haven.SetField("SMA", 6600)
haven.SetField("ECC", 0.01)
haven.SetField("INC", 78)
haven.SetField("RAAN", 45)
haven.SetField("AOP", 90)
haven.SetField("TA", 180)

# Spacecraft ballistic properties for the SRP and Drag models
haven.SetField("SRPArea", 2.5)
haven.SetField("Cr", 1.75)
haven.SetField("DragArea", 2.0)
haven.SetField("Cd", 2.2)
haven.SetField("DryMass", 100)

# Force model settings
force_model = gmat.Construct("ForceModel", "force_model")
force_model.SetField("CentralBody", "Earth")

# An 8x8 JGM-3 Gravity Model.  No name set, so management resides with the user
graviation_potential_file = os.path.join(GMAT_BASE_PATH, 'data/gravity/earth/JGM3.cof')
earth_gravity_model = gmat.Construct("GravityField")
earth_gravity_model.SetField("BodyName","Earth")
earth_gravity_model.SetField("PotentialFile", graviation_potential_file)
earth_gravity_model.SetField("Degree",8)
earth_gravity_model.SetField("Order",8)

# Add force to the dynamics model.  Here we pass ownership to the force model
force_model.AddForce(earth_gravity_model)

# Setup the state vector used for the force, connecting the spacecraft
propagation_state_manager = gmat.PropagationStateManager()
propagation_state_manager.SetObject(haven)
propagation_state_manager.BuildState()

# Finish the object connection
force_model.SetPropStateManager(propagation_state_manager)
force_model.SetState(propagation_state_manager.GetState())

# Assemble all of the objects together 
gmat.Initialize()

# Finish force model setup:
##  Map the spacecraft state into the model
force_model.BuildModelFromMap()
##  Load the physical parameters needed for the forces
force_model.UpdateInitialData()

# # Now access the state and get the derivative data
# pstate = haven.GetState().GetState()
# print("State Vector: ", pstate)
# print()

# force_model.GetDerivatives(pstate)
# dv = force_model.GetDerivativeArray()
# print("Derivative:   ", dv)
# print()

# vec = force_model.GetDerivativesForSpacecraft(haven)
# print("SCDerivative: ", vec)
# print()

# Build the propagation container class 
propagator_container = gmat.Construct("Propagator","pd_propagator_container")

# Create and assign a numerical integrator for use in the propagation (Prince-Dormand-78)
pd_propagator = gmat.Construct("PrinceDormand78", "pd_propagator")
propagator_container.SetReference(pd_propagator)

# Assign the force model imported from BasicFM
propagator_container.SetReference(force_model)

# Set some of the fields for the integration
propagator_container.SetField("InitialStepSize", 60.0)
propagator_container.SetField("Accuracy", 1.0e-12)
propagator_container.SetField("MinStep", 0.0)

# Perform top level initialization
gmat.Initialize()

# Setup the spacecraft that is propagated
propagator_container.AddPropObject(haven)
propagator_container.PrepareInternals()

# Refresh the pd_propagator reference
pd_propagator = propagator_container.GetPropagator()

# Take 60 second steps, showing the state before and after
time = 0.0
step = 60.0
print(time, " sec, state = ", pd_propagator.GetState())


#%% Setup Propagation Loop
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation

# Propagate for 1 day (1440 mins/day, 1-min step size)
PROPAGATION_DURATION = 1400

t = []
position_x = []
position_y = []
position_z = []

figure = plt.figure()
axis = figure.gca(projection='3d')
plt.title('Haven - Inertial View')
plt.xlabel('X-Position')
plt.ylabel('Y-Position')
# Draw Earth
u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
RADIUS_EARTH = 6378
x = RADIUS_EARTH * np.cos(u)*np.sin(v)
y = RADIUS_EARTH * np.sin(u)*np.sin(v)
z = RADIUS_EARTH * np.cos(v)
axis.plot_wireframe(x, y, z, color="b", rstride=1, cstride=1)
# xlist=np.linspace(-1.0,1.0,50)
# ylist=np.linspace(-1.0,1.0,50)
# r=np.linspace(1.0,1.0,50)
# X,Y= np.meshgrid(xlist,ylist)
# Z=np.sqrt(r**2-X**2-Y**2) 
# # Now plot the bottom half
# cp=axis.plot_wireframe(X*6378,Y*6378,Z*6378,color="b")
# cp=axis.plot_wireframe(X*6378,Y*6378,-Z*6378,color="b") 
# plt.show()

for x in range(PROPAGATION_DURATION):

    haven_position = pd_propagator.GetState()[:3]
    haven_velocity = pd_propagator.GetState()[3:]

    t.append(time)
    position_x.append(haven_position[0])
    position_y.append(haven_position[1])
    position_z.append(haven_position[2])

    axis.plot(position_x, position_y, position_z, 'r', label='parametric curve')
    
    plt.pause(0.1)


    pd_propagator.Step(step)
    time = time + step

    #####################################
    #   #   #   DO STUFF HERE   #   #   #
    #####################################

    print(time, " sec, state = ", pd_propagator.GetState())



